# frozen_string_literal: true

class Board
  attr_reader :visual_board, :player1_moves, :player2_moves

  @@SIZE = 9

  def initialize
    @visual_board = (1..@@SIZE).to_a
    @player1_moves = []
    @player2_moves = []
  end

  public

  def game_over?(winning_positions)
    get_board_winner(winning_positions) || board_filled? ? true : false
  end

  def place_piece(user_position, piece, player)
    unless position_filled?(user_position)
      visual_board[user_position - 1] = piece

      if player == 1
        @player1_moves.push(user_position)
      else
        @player2_moves.push(user_position)
      end
      
      return true
    end
    false
  end

  def get_board_winner(winning_positions)
    winning_positions.each do |positions|
      if positions & @player1_moves == positions
        return 1
      elsif positions & @player2_moves == positions
        return 2
      end
    end
    false
  end

  private

  def board_filled?
    (@player1_moves.size + @player2_moves.size) >= @@SIZE
  end

  def position_filled?(position)
    @player1_moves.include?(position) || @player2_moves.include?(position) 
  end
end
