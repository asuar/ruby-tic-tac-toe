# frozen_string_literal: true

require './board.rb'

describe Board do
  subject(:board) { Board.new }

  describe '.game_over?' do
    context 'when player 1 completes a row' do
      it 'ends the game' do
        board.place_piece(1, 'X', 1)
        board.place_piece(2, 'X', 1)
        board.place_piece(3, 'X', 1)
        should be_game_over([[1, 2, 3]])
      end
    end

    context 'when player 2 completes a row' do
      it 'ends the game' do
        board.place_piece(1, 'O', 2)
        board.place_piece(2, 'O', 2)
        board.place_piece(3, 'O', 2)
        should be_game_over([[1, 2, 3]])
      end
    end

    context 'when the board is full' do
      it 'ends the game' do
        board.place_piece(1, 'X', 1)
        board.place_piece(2, 'X', 1)
        board.place_piece(3, 'O', 2)

        board.place_piece(4, 'O', 2)
        board.place_piece(5, 'O', 2)
        board.place_piece(6, 'X', 1)

        board.place_piece(7, 'X', 1)
        board.place_piece(8, 'O', 2)
        board.place_piece(9, 'X', 1)

        should be_game_over([[]])
      end
    end

    context 'when the board is empty' do
      it 'does not end the game' do
        should_not be_game_over([[1]])
      end
    end
  end

  describe '.place_piece' do
    context 'when position is empty' do
      it 'places the piece' do
        expect(board.place_piece(1, 'X', 1)).to be true
      end
    end

    context 'when position is filled' do
      it "doesn't place the piece" do
        board.place_piece(1, 'X', 1)
        expect(board.place_piece(1, 'X', 1)).to be false
      end
    end

    it 'handles player 1 placing a piece' do
      expect(board.place_piece(1, 'X', 1)).to be true
    end

    it 'handles player 2 placing a piece' do
      board.place_piece(1, 'X', 1)
      expect(board.place_piece(2, 'O', 2)).to be true
    end
  end

  describe '.get_board_winner' do
    it 'handles player 1 completing a row' do
      board.place_piece(1, 'X', 1)
      board.place_piece(2, 'X', 1)
      board.place_piece(3, 'X', 1)
      expect(board.get_board_winner([[1, 2, 3]])).to be 1
    end

    it 'handles player 2 completing a row' do
      board.place_piece(1, 'O', 2)
      board.place_piece(2, 'O', 2)
      board.place_piece(3, 'O', 2)
      expect(board.get_board_winner([[1, 2, 3]])).to be 2
    end

    it 'handles ties' do
      expect(board.get_board_winner([[1, 2, 3]])).to be false
    end
  end
end
