# frozen_string_literal: true

require './tic_tac_toe.rb'

describe TicTacToe do
  subject(:game) { TicTacToe.new }
  let(:board) { game.board }

  describe '.game_over?' do
    context 'when row is filled' do
      it 'ends the game' do
        board.place_piece(1, 'X', 1)
        board.place_piece(2, 'X', 1)
        board.place_piece(3, 'X', 1)
        should be_game_over
      end
    end

    context 'when column is filled' do
      it 'ends game' do
        board.place_piece(1, 'X', 1)
        board.place_piece(4, 'X', 1)
        board.place_piece(7, 'X', 1)
        should be_game_over
      end
    end

    context 'when diagonal is filled' do
      it 'ends game' do
        board.place_piece(1, 'X', 1)
        board.place_piece(5, 'X', 1)
        board.place_piece(9, 'X', 1)
        should be_game_over
      end
    end

    context 'when board is filled' do
      it 'ends game' do
        board.place_piece(1, 'X', 1)
        board.place_piece(2, 'X', 1)
        board.place_piece(3, 'O', 2)

        board.place_piece(4, 'O', 2)
        board.place_piece(5, 'O', 2)
        board.place_piece(6, 'X', 1)

        board.place_piece(7, 'X', 1)
        board.place_piece(8, 'O', 2)
        board.place_piece(9, 'X', 1)
        should be_game_over
      end
    end
  end

  describe '.declare_winner' do
    context 'when player 1 is the winner' do
    it 'declares player 1 the winner' do
      player1 = double('player1')
      allow(player1).to receive(:to_s) { 'Alain' }
      game.player1 = player1.to_s

      board.place_piece(1, 'X', 1)
      board.place_piece(2, 'X', 1)
      board.place_piece(3, 'X', 1)
      expect(game.declare_winner).to eq('Player Alain has won!')
    end
end

context 'when player 2 is the winner' do
    it 'declares player 2 the winner' do
      player2 = double('player1')
      allow(player2).to receive(:to_s) { 'Alain' }
      game.player2 = player2.to_s

      board.place_piece(1, 'O', 2)
      board.place_piece(2, 'O', 2)
      board.place_piece(3, 'O', 2)
      expect(game.declare_winner).to eq('Player Alain has won!')
    end
end

    context 'when there is a tie' do
      it 'declares no winner' do
        board.place_piece(1, 'X', 1)
        board.place_piece(2, 'X', 1)
        board.place_piece(3, 'O', 2)

        board.place_piece(4, 'O', 2)
        board.place_piece(5, 'O', 2)
        board.place_piece(6, 'X', 1)

        board.place_piece(7, 'X', 1)
        board.place_piece(8, 'O', 2)
        board.place_piece(9, 'X', 1)
        expect(game.declare_winner).to eq('No winner! :(')
      end
    end
  end

  describe '.play_next_turn' do
    it "handles player 1's turn" do
      player1 = double('player1')
      allow(player1).to receive(:to_s) { 'Alain' }
      game.player1 = player1.to_s

      allow_any_instance_of(Object).to receive(:gets).and_return '1'
      expect { game.play_next_turn }.to output.to_stdout
      expect(board.player1_moves).to eq([1])
    end

    it "handles player 2's turn" do
      player1 = double('player1')
      allow(player1).to receive(:to_s) { 'Alain' }
      game.player1 = player1.to_s

      player2 = double('player1')
      allow(player2).to receive(:to_s) { 'Cami' }
      game.player2 = player2.to_s

      allow_any_instance_of(Object).to receive(:gets).and_return '1'
      expect { game.play_next_turn }.to output.to_stdout
      allow_any_instance_of(Object).to receive(:gets).and_return '2'
      expect { game.play_next_turn }.to output.to_stdout
      expect(board.player2_moves).to eq([2])
    end
  end
end
