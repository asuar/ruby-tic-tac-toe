# frozen_string_literal: true

class Game
  require_relative 'tic_tac_toe.rb'

  def initialize(game)
    setup(game)
  end

  public

  def play_game
    @game.play_next_turn until @game.game_over?
    end_game
  end

  private

  def setup(game)
    case game
    when 'tic-tac-toe'
      @game = TicTacToe.new
    else
      puts 'Error: Unknown Game!'
    end
    @game.show_starting_screen
    @game.initialize_players
  end

  def play_again?
    user_input = ''
    while user_input != 'Y' && user_input != 'N'
      puts 'Want to play again? [Y/N]'
      user_input = gets.chomp.upcase
    end
    user_input == 'Y'
  end

  def end_game
    @game.display_board
    puts @game.declare_winner
    if play_again?
      setup(@game.to_s)
      play_game
    else
      puts 'Thanks for playing!'
    end
  end
end

my_game = Game.new('tic-tac-toe')
my_game.play_game
