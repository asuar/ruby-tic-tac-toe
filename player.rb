# frozen_string_literal: true

class Player
  def initialize(name)
    @name = name
  end

  public

  def to_s
    @name
  end
end
