# frozen_string_literal: true

class TicTacToe
  require_relative 'player.rb'
  require_relative 'board.rb'

  @@WINNING_POSITIONS = [[1, 2, 3], [4, 5, 6], [7, 8, 9], [1, 4, 7], [2, 5, 8], [3, 6, 9], [1, 5, 9], [3, 5, 7]]

  attr_reader :board
  attr_accessor :player1, :player2

  def initialize
    @board = Board.new
    @turn_count = 0
  end

  public

  def to_s
    'tic-tac-toe'
  end

  def show_starting_screen
    puts '*******************************'
    puts '* Tic-Tac-Toe by Alain Suarez *'
    puts '*******************************'
    puts "* One player is X's while the *"
    puts "* other is O's. The first     *"
    puts '* to get 3 of their marks in  *'
    puts '* a row (up, down, across, or *'
    puts '* diagonally) is the winner.  *'
    puts '*******************************'
  end

  def display_board
    visual_board = @board.visual_board

    side_length = Integer.sqrt(visual_board.size)
    visual_board.each_with_index do |value, index|
      if index % side_length == 0
        print " #{value} |"
      elsif index % side_length == 2
        puts "| #{value} "
        puts '---+---+---' if (index + 1) < visual_board.size
      else
        print " #{value} "
      end
    end
  end

  def game_over?
    @board.game_over?(@@WINNING_POSITIONS)
  end

  def play_next_turn
    @turn_count += 1
    puts "Turn #{@turn_count}!"
    display_board
    current_player = @turn_count.odd? ? @player1 : @player2
    user_position = 0
    piece_placed = false
    while user_position < 1 || user_position > 9 || !piece_placed
      puts "Enter a number to place your piece #{current_player}!"
      user_position = gets.chomp.to_i
      piece_placed = @board.place_piece(user_position, current_player == @player1 ? 'X' : 'O',  current_player == @player1 ? 1 : 2)
    end
  end

  def declare_winner
    if game_over?
      board_winner = @board.get_board_winner(@@WINNING_POSITIONS)
      if board_winner
        return "Player #{board_winner == 1 ? @player1.to_s : @player2.to_s} has won!"
      end
    end
    'No winner! :('
  end

  def initialize_players
    puts "What is the name of the player who will use X's?"
    @player1 = Player.new(gets.chomp.capitalize)
    puts "What is the name of the player who will use O's?"
    @player2 = Player.new(gets.chomp.capitalize)

    if @player1.to_s == @player2.to_s
      @player1 = Player.new("#{@player1} 1")
      @player2 = Player.new("#{@player2} 2")
    end
  end

end
